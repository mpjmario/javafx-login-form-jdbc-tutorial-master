package com.javaguides.javafx.login;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
//import javafx.scene.control.Hyperlink;
import javafx.stage.Stage;


public class MainLoginApp extends Application {

    @Override
    public void start(Stage stage) throws Exception {
    	System.out.println(getClass());
    	Parent root = FXMLLoader.load(getClass().getResource("login_form.fxml")); 	
    	
    	stage.setTitle("User Login");
        stage.setScene(new Scene(root, 400, 450));
        stage.show();
    }
    
    public static void main(String[] args) {
        launch(args);
    }

}
