package com.javaguides.javafx.login;

import java.sql.SQLException;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

public class LoginController {    
    @FXML
    private TextField emailIdField;   
    @FXML
    private PasswordField passwordField;    
    @FXML
    private Button submitButton;  
    @FXML
    private Label lbl1,lbl2;
    
    @FXML
    private Button btnGoRegister,btnGoLogin;
    
    @FXML
    
    private Button btnExit;
     
    @FXML
    public void login(ActionEvent event) throws SQLException{    
    	Window owner = submitButton.getScene().getWindow();
    	
    	System.out.println(emailIdField.getText());
    	System.out.println(passwordField.getText());
    	
    	if(emailIdField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter your email id");
            return;
        }
        if(passwordField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter a password");
            return;
        }
        
    	String emailId = emailIdField.getText();
    	String password = passwordField.getText();
    	
    	JdbcDaoLogin jdbcDaoLogin = new JdbcDaoLogin();
    	boolean flag = jdbcDaoLogin.validate(emailId, password);

    	if(!flag) {
    		infoBox("Please enter correct Email and Password", null, "Failed");
    	}else {
    		infoBox("Login Successful!", null, "Failed");
    	 }      
    	 }
    
    public static void infoBox(String infoMessage, String headerText, String title){
        Alert alert = new Alert(AlertType.CONFIRMATION);
        alert.setContentText(infoMessage);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.showAndWait();
    }
    
    private static void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
    
    @FXML
    private void handleButtonAction1 (ActionEvent event) throws Exception {
         Stage stage ;
         Parent root ;
       
        if(event.getSource()==btnGoRegister){
            stage = (Stage) btnGoRegister.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("registration_form.fxml"));          
        }      
        else {
            stage = (Stage) btnGoLogin.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("login_form.fxml"));           
        }
        Scene scene = new Scene(root);
        stage.setScene(scene);
        stage.show();
    }
    
    @FXML
	public void exitWindows(ActionEvent event) {
		if(event.getSource()==btnExit) { 
		System.exit(0);
		}
	}
}
