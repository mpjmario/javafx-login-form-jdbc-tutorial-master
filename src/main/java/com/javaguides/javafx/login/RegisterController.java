package com.javaguides.javafx.login;

import java.sql.SQLException;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

public class RegisterController {
    
    @FXML
    private TextField fullNameField;
    
    @FXML
    private TextField emailIdField;
    
    @FXML
    private PasswordField passwordField;
    
    @FXML
    private Button submitButton; 
    
    @FXML
    private Label lbl1,lbl2;
    
    @FXML
    private Button btnGoRegister,btnGoLogin;
       
    @FXML
    public void register(ActionEvent event) throws SQLException{
    
    	Window owner = submitButton.getScene().getWindow();
    	
    	System.out.println(fullNameField.getText());
    	System.out.println(emailIdField.getText());
    	System.out.println(passwordField.getText());
    	if(fullNameField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter your name");
            return;
        }
    	
    	if(emailIdField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter your email id");
            return;
        }
        if(passwordField.getText().isEmpty()) {
            showAlert(Alert.AlertType.ERROR, owner, "Form Error!", 
                    "Please enter a password");
            return;
        }
        
    	String fullName = fullNameField.getText();
    	String emailId = emailIdField.getText();
    	String password = passwordField.getText();
    	
    	JdbcDao jdbcDao = new JdbcDao();
    	jdbcDao.insertRecord(fullName, emailId, password);
    	
    	showAlert(Alert.AlertType.CONFIRMATION, owner, "Registration Successful!", 
                "Welcome " + fullNameField.getText());
    }
    
    private static void showAlert(Alert.AlertType alertType, Window owner, String title, String message) {
        Alert alert = new Alert(alertType);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(message);
        alert.initOwner(owner);
        alert.show();
    }
    
    //
    
    @FXML
    private void handleButtonAction2 (ActionEvent event) throws Exception {
      //  Stage stage;
      //  Parent root;
       
        if(event.getSource()== btnGoLogin){
         Stage   stage = (Stage) btnGoLogin.getScene().getWindow();
          Parent  root = FXMLLoader.load(getClass().getResource("login_form.fxml"));
          Scene scene = new Scene(root);
          stage.setScene(scene);
          stage.show();
        }
        else {
           Stage stage = (Stage) btnGoRegister.getScene().getWindow();
           Parent root = FXMLLoader.load(getClass().getResource("registration_form.fxml"));
           Scene scene = new Scene(root);
           stage.setScene(scene);
           stage.show();
        }
        //Scene scene = new Scene(root);
       // stage.setScene(scene);
        //stage.show();
    }
   
}
