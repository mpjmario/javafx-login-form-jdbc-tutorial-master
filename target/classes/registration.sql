CREATE DATABASE  IF NOT EXISTS `javafx_registration`;
USE `javafx_registration`;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `registration`;

CREATE TABLE `registration` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `full_name` varchar(250) NOT NULL,
  `email_id` varchar(250) NOT NULL,
  `password` varchar(250) ,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Data for table `registration`
--

INSERT INTO `registration` VALUES 
	(1,'Leslie Andrews','leslie@luv2code.com', 'root'),
	(2,' Emma Baumgarten','emma@luv2code.com', '0922mp00'),
	(3,'Avani Gupta','avani@luv2code.com','M0922mp00@'),
	(4,'Yuri Petrov','yuri@luv2code.com','root'),
	(5,'Juan Vega','juan@luv2code.com','0922mp00');

